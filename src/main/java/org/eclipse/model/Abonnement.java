package org.eclipse.model;

import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class Abonnement {
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@Column(name = "idAbonnement", updatable = false, nullable = false)
	private Integer idAbonnement;
	private String nomAbonnement;
	private Date dateAbonnement;
		
	@OneToOne(cascade= {CascadeType.PERSIST, CascadeType.REMOVE})
	@JoinColumn(name="idClient", referencedColumnName="idClient", nullable=false)
	private Client client;
	
	@OneToMany(cascade= {CascadeType.PERSIST, CascadeType.REMOVE})
	private List<Facture> lesFactures = new ArrayList<Facture>();
	
	public Abonnement(String nomAbonnement, Date dateAbonnement, Client client,
			List<Facture> lesFactures) {
		super();
		this.nomAbonnement = nomAbonnement;
		this.dateAbonnement = dateAbonnement;
		this.client = client;
		this.lesFactures = lesFactures;
	}
	public Abonnement() {
		super();
	}
	public Integer getIdAbonnement() {
		return idAbonnement;
	}
	public void setIdAbonnement(Integer idAbonnement) {
		this.idAbonnement = idAbonnement;
	}
	public String getNomAbonnement() {
		return nomAbonnement;
	}
	public void setNomAbonnement(String nomAbonnement) {
		this.nomAbonnement = nomAbonnement;
	}
	public Date getDateAbonnement() {
		return dateAbonnement;
	}
	public void setDateAbonnement(Date dateAbonnement) {
		this.dateAbonnement = dateAbonnement;
	}

	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public List<Facture> getLesFactures() {
		return lesFactures;
	}
	public void setLesFactures(List<Facture> lesFactures) {
		this.lesFactures = lesFactures;
	}	
	
	public boolean addFacture(Facture facture) {
		return lesFactures.add(facture);
	}
	public boolean removeFacture(Facture facture) {
		return lesFactures.remove(facture);
	}	
	
	@Override
	public String toString() {
		return "Abonnement [idAbonnement=" + idAbonnement + ", nomAbonnement=" + nomAbonnement + ", dateAbonnement="
				+ dateAbonnement + ", client=" + client + ", lesFactures=" + lesFactures + "]";
	}
	
	
}
