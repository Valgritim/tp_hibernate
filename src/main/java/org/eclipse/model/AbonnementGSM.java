package org.eclipse.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;

@Entity
public class AbonnementGSM extends Abonnement{
	
	private int fidelio;

	public AbonnementGSM() {
		super();
	}

	public int getFidelio() {
		return fidelio;
	}

	public void setFidelio(int fidelio) {
		this.fidelio = fidelio;
	}

	@Override
	public String toString() {
		return "AbonnementGSM [fidelio=" + fidelio + "]";
	}




	
	
	

}
