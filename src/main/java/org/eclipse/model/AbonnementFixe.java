package org.eclipse.model;

import javax.persistence.Entity;

@Entity
public class AbonnementFixe extends Abonnement{
	
		private int debit;

		public AbonnementFixe() {
			super();
			
		}
		

		public int getDebit() {
			return debit;
		}

		public void setDebit(int debit) {
			this.debit = debit;
		}

		@Override
		public String toString() {
			return "AbonnementFixe [debit=" + debit + "]";
		}
		
		

}
