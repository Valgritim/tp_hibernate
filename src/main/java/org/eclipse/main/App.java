package org.eclipse.main;

import java.util.Date;
import java.util.*;

import org.eclipse.config.HibernateUtil;
import org.eclipse.dao.AbonnementDao;
import org.eclipse.dao.ClientDao;
import org.eclipse.dao.FactureDao;
import org.eclipse.model.Abonnement;
import org.eclipse.model.AbonnementFixe;
import org.eclipse.model.AbonnementGSM;
import org.eclipse.model.Client;
import org.eclipse.model.Facture;
import org.hibernate.Session;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
    	Session session = HibernateUtil.getSessionFactory().openSession();
    	
//        Client client1 = new Client();
//        client1.setNom("Dao");
//        client1.setEmail("etienne@gmail.com");
//        client1.setTel("0606060606");
//        client1.setVille("Rome");
//        
//        Client client2 = new Client();
//        client2.setNom("Einstein");
//        client2.setEmail("albert@cnrs.com");
//        client2.setTel("0909090909");
//        client2.setVille("Paris");
//        
//        //Factures abo1        
//        Facture facture1 = new Facture();
//        facture1.setDateFact(new Date("31/01/2020"));
//        facture1.setMontant(50.30f);
//        facture1.setReglee(true);
//        
//        Facture facture2 = new Facture();
//        facture2.setDateFact(new Date("31/01/2020"));
//        facture2.setMontant(15.50f);
//        facture2.setReglee(true);
//        
//        Facture facture3 = new Facture();
//        facture3.setDateFact(new Date("30/12/2019"));
//        facture3.setMontant(48.30f);
//        facture3.setReglee(true);
//        
//        Facture facture4 = new Facture();
//        facture4.setDateFact(new Date("30/12/2019"));
//        facture4.setMontant(7.10f);
//        facture4.setReglee(true);
//        
//                
//        ClientDao clientdao = new ClientDao(session);
//        clientdao.save(client1);
//        clientdao.save(client2);
//
//        FactureDao facturedao = new FactureDao(session);
//        facturedao.save(facture1);
//        facturedao.save(facture2);
//        facturedao.save(facture3);
//        facturedao.save(facture4);
//        
//        Abonnement abonnement1 = new Abonnement();
//        abonnement1.setNomAbonnement("Révolution");
//        abonnement1.setDateAbonnement(new Date("01/10/2015"));
//        abonnement1.setClient(client1);
//        
//            
//        Abonnement abonnement2 = new Abonnement();
//        abonnement2.setNomAbonnement("Delta");
//        abonnement2.setDateAbonnement(new Date("01/12/2016"));
//        abonnement2.setClient(client2);
//        
//        abonnement1.addFacture(facture1);
//        abonnement1.addFacture(facture2);
//        abonnement2.addFacture(facture3);
//        abonnement2.addFacture(facture4);    
//
//
//        
//        AbonnementDao abonnementdao = new AbonnementDao(session);
//        abonnementdao.save(abonnement1);
//        abonnementdao.save(abonnement2);
    	
    	Client client = new Client();
//    	ClientDao clientdao = new ClientDao(session);
//    	client = clientdao.findById(1);
//    	System.out.println(client);    	
    	
//    	AbonnementGSM abogsm = new AbonnementGSM();
//    	abogsm.setFidelio(150);
//    	abogsm.setDateAbonnement(new Date("25/01/2018"));
//    	abogsm.setNomAbonnement("Upsilon");
//    	abogsm.setClient(client);
    	
//    	AbonnementFixe aboFixe = new AbonnementFixe();
//    	aboFixe.setClient(client);
//    	aboFixe.setDateAbonnement(new Date("25/01/2016"));
//    	aboFixe.setDebit(30);
//    	aboFixe.setNomAbonnement("Pack fantastique");
    	
    	List<Abonnement> abonnement = new ArrayList<Abonnement>();
    	AbonnementDao abodao = new AbonnementDao(session);
    	abodao.findAll();
    	
    	
        
    }
}
